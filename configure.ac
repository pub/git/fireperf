AC_PREREQ([2.71])
AC_INIT(
	[fireperf],
	[0.2.0],
	[info@ipfire.org],
	[fireperf],
	[https://www.ipfire.org/]
)

AC_CONFIG_SRCDIR([src/main.c])
AC_CONFIG_AUX_DIR([build-aux])
AM_INIT_AUTOMAKE([
	foreign
	1.11
	-Wall
	-Wno-portability
	silent-rules
	tar-pax
	no-dist-gzip
	dist-xz
	subdir-objects
])
AC_PROG_CC
AC_USE_SYSTEM_EXTENSIONS
AC_SYS_LARGEFILE
AC_CONFIG_MACRO_DIR([m4])
AM_SILENT_RULES([yes])
LT_INIT([
	disable-static
	pic-only
])
AC_PREFIX_DEFAULT([/usr])

IT_PROG_INTLTOOL([0.40.0])

GETTEXT_PACKAGE=${PACKAGE_TARNAME}
AC_SUBST(GETTEXT_PACKAGE)

AC_PROG_SED
AC_PROG_MKDIR_P

# - man ------------------------------------------------------------------------

have_man_pages=no
AC_ARG_ENABLE(man_pages, AS_HELP_STRING([--disable-man-pages],
	[do not install man pages]))
AS_IF([test "x$enable_man_pages" != xno], [have_man_pages=yes])
AM_CONDITIONAL(ENABLE_MAN_PAGES, [test "x$have_man_pages" = "xyes"])

AC_PATH_PROG([XSLTPROC], [xsltproc])

AC_CHECK_PROGS(ASCIIDOC, [asciidoc])
if test "${have_man_pages}" = "yes" && test -z "${ASCIIDOC}"; then
	AC_MSG_ERROR([Required program 'asciidoc' not found])
fi

# - debug ----------------------------------------------------------------------

AC_ARG_ENABLE([debug],
        AS_HELP_STRING([--enable-debug], [enable debug messages @<:@default=disabled@:>@]),
        [], [enable_debug=no])
AS_IF([test "x$enable_debug" = "xyes"], [
        AC_DEFINE(ENABLE_DEBUG, [1], [Debug messages.])
])

AC_CHECK_HEADERS_ONCE([
	arpa/inet.h \
	getopt.h \
	netinet/in.h \
	stdio.h \
	stdlib.h \
	string.h \
])

AC_CHECK_FUNCS([ \
])

# C Compiler
AC_PROG_CC

CC_CHECK_FLAGS_APPEND([with_cflags], [CFLAGS], [\
	-std=gnu11 \
	-Wall \
	-Wextra \
	-Warray-bounds=2 \
	-Wdate-time \
	-Wendif-labels \
	-Werror=format=2 \
	-Werror=format-signedness \
	-Werror=implicit-function-declaration \
	-Werror=implicit-int \
	-Werror=incompatible-pointer-types \
	-Werror=int-conversion \
	-Werror=missing-declarations \
	-Werror=missing-prototypes \
	-Werror=overflow \
	-Werror=override-init \
	-Werror=return-type \
	-Werror=shift-count-overflow \
	-Werror=shift-overflow=2 \
	-Werror=strict-flex-arrays \
	-Werror=undef \
	-Wfloat-equal \
	-Wimplicit-fallthrough=5 \
	-Winit-self \
	-Wlogical-op \
	-Wmissing-include-dirs \
	-Wmissing-noreturn \
	-Wnested-externs \
	-Wold-style-definition \
	-Wpointer-arith \
	-Wredundant-decls \
	-Wshadow \
	-Wstrict-aliasing=2 \
	-Wstrict-prototypes \
	-Wsuggest-attribute=noreturn \
	-Wunused-function \
	-Wwrite-strings \
	-Wzero-length-bounds \
	-Wno-unused-parameter \
	-Wno-missing-field-initializers \
	-fdiagnostics-show-option \
	-fno-common \
])

# Enable -fno-semantic-interposition (if available)
CC_CHECK_FLAGS_APPEND([with_cflags], [CFLAGS], [-fno-semantic-interposition])
CC_CHECK_FLAGS_APPEND([with_ldflags], [LDFLAGS], [-fno-semantic-interposition])

AC_SUBST([OUR_CFLAGS],  $with_cflags)
AC_SUBST([OUR_LDFLAGS], $with_ldflags)

# - libraries ------------------------------------------------------------------

PKG_CHECK_MODULES([CURSES], [ncurses])
PKG_CHECK_MODULES([URING], [liburing])

# ------------------------------------------------------------------------------

AC_CONFIG_HEADERS(config.h)
AC_CONFIG_FILES([
        Makefile
        po/Makefile.in
])

AC_OUTPUT
AC_MSG_RESULT([
        $PACKAGE $VERSION
        =====

        prefix:                 ${prefix}
        sysconfdir:             ${sysconfdir}
        libdir:                 ${libdir}
        includedir:             ${includedir}

        compiler:               ${CC}
        cflags:                 ${with_cflags}
        ldflags:                ${with_ldflags}

        debug:                  ${enable_debug}
])
