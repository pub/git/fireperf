/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <unistd.h>

#include "client.h"
#include "constants.h"
#include "ctx.h"
#include "logging.h"
#include "main.h"
#include "random.h"
#include "util.h"

struct fireperf_client_state {
	// For the timeout
	int timerfd;

	// How many open connections?
	unsigned int connections;
};

static int setup_timer(struct fireperf_ctx* ctx, int epollfd) {
	int timerfd = -1;
	int r;

	// Create timerfd()
	timerfd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK|TFD_CLOEXEC);
	if (timerfd < 0)
		return -errno;

	struct epoll_event ev = {
		.events  = EPOLLIN,
		.data.fd = timerfd,
	};

	// Register the timer with the event loop
	r = epoll_ctl(epollfd, EPOLL_CTL_ADD, timerfd, &ev);
	if (r)
		return -errno;

	// Let the timer ping us once a second
	struct itimerspec timer = {
		.it_value.tv_sec = ctx->timeout,
	};

	// Arm the timer
	r = timerfd_settime(timerfd, 0, &timer, NULL);
	if (r)
		return -errno;

	return timerfd;
}

static int open_connection(struct fireperf_ctx* ctx,
		struct fireperf_client_state* state, int epollfd) {
	struct fireperf_worker* worker = NULL;
	int fd;
	int r;

	// Open a new socket
	fd = socket(AF_INET6, SOCK_STREAM|SOCK_NONBLOCK|SOCK_CLOEXEC, 0);
	if (fd < 0) {
		ERROR(ctx, "Could not open socket: %m\n");
		r = -errno;
		goto ERROR;
	}

	DEBUG(ctx, "Opening socket %d (port %d)...\n", fd, ctx->port);

	// Define the peer
	struct sockaddr_in6 peer = {
		.sin6_family = AF_INET6,
		.sin6_addr = ctx->address,
		.sin6_port = htons(ctx->port),
	};

	// Set socket buffer sizes
	r = set_socket_buffer_sizes(ctx, fd);
	if (r)
		goto ERROR;

	// Connect to the server
	r = connect(fd, &peer, sizeof(peer));
	if (r) {
		switch (errno) {
			case EINPROGRESS:
				break;

			default:
				ERROR(ctx, "Could not connect to server: %m\n");
				r = -errno;
				goto ERROR;
		}
	}

	// Find a worker to delegate this connection to
	worker = fireperf_ctx_fetch_worker(ctx);

	// Close the connection if we could not find a worker
	if (!worker) {
		ERROR(ctx, "Could not find a worker that could handle a new connection\n");
		r = -EBUSY;
		goto ERROR;
	}

	int events = 0;

	// Let us know when the socket is ready for receiving data
	if (!ctx->keepalive_only)
		events |= FIREPERF_WORKER_RECV;

	// In duplex mode, we send data, too
	if (ctx->duplex)
		events |= FIREPERF_WORKER_SEND;

	// Delegate the connection
	r = fireperf_worker_delegate(worker, events, fd);
	if (r < 0) {
		ERROR(ctx, "Could not delegate a new connection to a worker: %s\n", strerror(-r));
		goto ERROR;
	}

	// Increment the number of open connections
	state->connections++;

	return 0;

ERROR:
	if (fd >= 0)
		close(fd);

	return r;
}

static int open_connections(struct fireperf_ctx* ctx,
		struct fireperf_client_state* state, int epollfd) {
	int r = 0;

	// If we don't have enough open connections, we try to open more
	while (state->connections < ctx->parallel) {
		r = open_connection(ctx, state, epollfd);
		if (r)
			break;
	}

	return r;
}

int fireperf_client_init(struct fireperf_ctx* ctx, void** data, int epollfd) {
	struct fireperf_client_state* state = NULL;
	int r;

	// Allocate state
	state = calloc(1, sizeof(*state));
	if (!state)
		return -errno;

	// Setup a timer for the timeout
	if (ctx->timeout) {
		state->timerfd = setup_timer(ctx, epollfd);
		if (state->timerfd < 0)
			return -state->timerfd;
	} else {
		state->timerfd = -1;
	}

	// Open some initial connections
	r = open_connections(ctx, state, epollfd);
	if (r)
		goto ERROR;

	// Return the state
	*data = state;

	return 0;

ERROR:
	if (state)
		fireperf_client_free(ctx, state);

	return r;
}

int fireperf_client_handle(struct fireperf_ctx* ctx, void* data, int fd) {
	struct fireperf_client_state* state = data;

	// Check if the timeout has fired
	if (state->timerfd == fd) {
		DEBUG(ctx, "Timeout reached\n");

		return -ETIME;
	}

	return 0;
}

void fireperf_client_free(struct fireperf_ctx* ctx, void* data) {
	struct fireperf_client_state* state = data;

	if (state->timerfd >= 0)
		close(state->timerfd);

	free(state);
}
