/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef FIREPERF_CLIENT_H
#define FIREPERF_CLIENT_H

#include "ctx.h"

int fireperf_client_init(struct fireperf_ctx* ctx, void** data, int epollfd);
int fireperf_client_handle(struct fireperf_ctx* ctx, void* data, int fd);
void fireperf_client_free(struct fireperf_ctx* ctx, void* data);

#endif /* FIREPERF_CLIENT_H */
