/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef FIREPERF_CONSTANTS_H
#define FIREPERF_CONSTANTS_H

#include <syslog.h>

#define DEFAULT_KEEPALIVE_COUNT                        3
#define DEFAULT_KEEPALIVE_INTERVAL                    10
#define DEFAULT_LOG_LEVEL                       LOG_INFO
#define DEFAULT_PARALLEL                               1
#define DEFAULT_PORT                                5001
#define DEFAULT_LISTENING_SOCKETS                      1

#define DEFAULT_RANDOM_POOL_SIZE	  (SOCKET_SEND_BUFFER_SIZE * 512)

#define MAX_PARALLEL                           (1 << 20)
#define MAX_WORKERS                                  128

// Socket buffer configuration
#define SOCKET_BUFFER_SIZE                   (64 * 1024)
#define SOCKET_RECV_BUFFER_SIZE       SOCKET_BUFFER_SIZE
#define SOCKET_SEND_BUFFER_SIZE       SOCKET_BUFFER_SIZE
#define SOCKET_BACKLOG                              1024

#define EPOLL_MAX_EVENTS                             128

#endif /* FIREPERF_CONSTANTS_H */
