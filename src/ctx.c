/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <netinet/in.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "ctx.h"
#include "logging.h"
#include "main.h"
#include "random.h"
#include "stats.h"

static int parse_address(const char* string, struct in6_addr* address6) {
	struct in_addr address4;

	// Try parsing this address
	int r = inet_pton(AF_INET6, string, address6);

	// Success!
	if (r == 1)
		return 0;

	// Try parsing this as an IPv4 address
	r = inet_pton(AF_INET, string, &address4);
	if (r == 1) {
		// Convert to IPv6-mapped address
		address6->s6_addr32[0] = htonl(0x0000);
		address6->s6_addr32[1] = htonl(0x0000);
		address6->s6_addr32[2] = htonl(0xffff);
		address6->s6_addr32[3] = address4.s_addr;

		return 0;
	}

	// Could not parse this
	return 1;
}

static int check_port(int port) {
	if (port <= 0 || port >= 65536) {
		fprintf(stderr, "Invalid port number: %u\n", port);

		return 2;
	}

	return 0;
}

static int parse_port_range(struct fireperf_ctx* ctx, const char* optarg) {
	int first_port, last_port;

	int r = sscanf(optarg, "%d:%d", &first_port, &last_port);
	if (r != 2)
		return 1;

	// Check if both ports are in range
	r = check_port(first_port);
	if (r)
		return r;

	r = check_port(last_port);
	if (r)
		return r;

	if (first_port > last_port) {
		fprintf(stderr, "Invalid port range: %s\n", optarg);
		return 2;
	}

	ctx->port = first_port;
	ctx->listening_sockets = (last_port - first_port) + 1;

	return 0;
}

static int parse_port(struct fireperf_ctx* ctx, const char* optarg) {
	ctx->port = atoi(optarg);
	ctx->listening_sockets = 1;

	return check_port(ctx->port);
}

static int parse_argv(struct fireperf_ctx* ctx, int argc, char* argv[]) {
	static struct option long_options[] = {
		{"client",     required_argument, 0, 'c'},
		{"close",      no_argument,       0, 'x'},
		{"debug",      no_argument,       0, 'd'},
		{"duplex",     no_argument,       0, 'D'},
		{"keepalive",  no_argument,       0, 'k'},
		{"parallel",   required_argument, 0, 'P'},
		{"port",       required_argument, 0, 'p'},
		{"server",     no_argument,       0, 's'},
		{"timeout",    required_argument, 0, 't'},
		{"version",    no_argument,       0, 'V'},
		{"zero",       no_argument,       0, 'z'},
		{0, 0, 0, 0},
	};

	int option_index = 0;
	int done = 0;

	while (!done) {
		int c = getopt_long(argc, argv, "c:dkp:st:xzDP:V", long_options, &option_index);

		// End
		if (c == -1)
			break;

		switch (c) {
			case 0:
				if (long_options[option_index].flag != 0)
					break;

				printf("option %s", long_options[option_index].name);

				if (optarg)
					printf("  with arg: %s", optarg);

				printf("\n");
				break;

			case '?':
				// getopt_long already printed the error message
				return 1;

			case 'V':
				printf("%s %s\n", PACKAGE_NAME, PACKAGE_VERSION);
				printf("Copyright (C) 2021 The IPFire Project (https://www.ipfire.org/)\n");
				printf("License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>\n");
				printf("This is free software: you are free to change and redistribute it\n");
				printf("There is NO WARRANTY, to the extent permitted by law.\n\n");
				printf("Written by Michael Tremer\n");

				exit(0);
				break;

			case 'c':
				ctx->mode = FIREPERF_MODE_CLIENT;

				// Parse the given IP address
				int r = parse_address(optarg, &ctx->address);
				if (r) {
					fprintf(stderr, "Could not parse IP address %s\n", optarg);
					return 2;
				}
				break;

			case 'D':
				ctx->duplex = 1;
				break;

			case 'd':
				ctx->loglevel = LOG_DEBUG;
				break;

			case 'k':
				ctx->keepalive_only = 1;
				break;

			case 'P':
				ctx->parallel = strtoul(optarg, NULL, 10);

				if (ctx->parallel > MAX_PARALLEL) {
					fprintf(stderr, "Number of parallel connections is too high: %lu\n",
						ctx->parallel);
					return 2;
				}
				break;

			case 'p':
				// Try parsing the port range first.
				// If this fails, we try parsing a single port
				r = parse_port_range(ctx, optarg);
				if (r == 1)
					r = parse_port(ctx, optarg);
				if (r)
					return r;

				break;

			case 's':
				ctx->mode = FIREPERF_MODE_SERVER;
				break;

			case 't':
				ctx->timeout = strtoul(optarg, NULL, 10);
				break;

			case 'x':
				ctx->close = 1;
				break;

			case 'z':
				ctx->zero = 1;
				break;

			default:
				done = 1;
				break;
		}
	}

	return 0;
}

int fireperf_ctx_create(struct fireperf_ctx** ctx, int argc, char* argv[]) {
	struct fireperf_ctx* c = NULL;
	int r;

	// Allocate a new context
	c = calloc(1, sizeof(*c));
	if (!c)
		return -errno;

	// Configure keepalive
	c->keepalive_count    = DEFAULT_KEEPALIVE_COUNT;
	c->keepalive_interval = DEFAULT_KEEPALIVE_INTERVAL;

	// Configure listening sockets
	c->listening_sockets  = DEFAULT_LISTENING_SOCKETS;

	// Configure logging
	c->loglevel           = DEFAULT_LOG_LEVEL;

	// Configure mode
	c->mode               = FIREPERF_MODE_NONE;

	// Configure port
	c->port               = DEFAULT_PORT;

	// Configure parallelism
	c->parallel           = DEFAULT_PARALLEL;

	// Fetch how many workers we would launch
	c->max_workers        = sysconf(_SC_NPROCESSORS_ONLN);

	// Set the default log callback
	fireperf_set_log_callback(c, NULL, NULL);

	// Parse the command line
	r = parse_argv(c, argc, argv);
	if (r)
		goto ERROR;

	// Initialize random pool
	if (!c->zero) {
		r = fireperf_random_pool_init(c);
		if (r)
			goto ERROR;
	}

	// Initialize the stats
	c->stats = fireperf_ctx_get_stats(c);

	// Return the context
	*ctx = c;

	return 0;

ERROR:
	if (c)
		fireperf_ctx_free(c);

	return r;
}

void fireperf_ctx_free(struct fireperf_ctx* ctx) {
	free(ctx);
}

static struct fireperf_worker* fireperf_ctx_find_idle_worker(struct fireperf_ctx* ctx) {
	const struct fireperf_stats* stats = NULL;

	for (unsigned int i = 0; i < ctx->num_workers; i++) {
		if (!ctx->workers[i])
			continue;

		// Fetch the worker's stats
		stats = fireperf_worker_get_stats(ctx->workers[i]);
		if (!stats)
			continue;

		// If the worker has no open connections, we can use it
		if (stats->open_connections == 0)
			return ctx->workers[i];
	}

	return NULL;
}

static struct fireperf_worker* fireperf_ctx_find_least_busy_worker(struct fireperf_ctx* ctx) {
	const struct fireperf_stats* stats = NULL;

	struct fireperf_worker* worker = NULL;
	unsigned int open_connections = 0;

	for (unsigned int i = 0; i < ctx->num_workers; i++) {
		if (!ctx->workers[i])
			continue;

		// Fetch the worker's stats
		stats = fireperf_worker_get_stats(ctx->workers[i]);
		if (!stats)
			continue;

		// If we have not seen a worker, we will save the first one;
		// or replace it if we found a better match
		if (!worker || stats->open_connections < open_connections) {
			worker = ctx->workers[i];

			// Save the number of connections
			open_connections = stats->open_connections;
		}
	}

	return worker;
}

/*
	This function returns a free worker
*/
struct fireperf_worker* fireperf_ctx_fetch_worker(struct fireperf_ctx* ctx) {
	struct fireperf_worker* worker = NULL;
	int r;

	// Return the first idle worker if available
	worker = fireperf_ctx_find_idle_worker(ctx);
	if (worker)
		return worker;

	// If all current workers are busy and we have not reached our maximum,
	// we create a new one.
	if (ctx->num_workers < ctx->max_workers) {
		// Create a new worker
		r = fireperf_worker_create(&worker, ctx);
		if (r < 0) {
			ERROR(ctx, "Could not create worker: %s\n", strerror(-r));
			goto ERROR;
		}

		// Launch the worker
		r = fireperf_worker_launch(worker);
		if (r < 0) {
			ERROR(ctx, "Could not launch worker: %s\n", strerror(-r));
			goto ERROR;
		}

		// Store a reference
		ctx->workers[ctx->num_workers++] = worker;

		return worker;
	}

	// Return the least busy worker
	worker = fireperf_ctx_find_least_busy_worker(ctx);
	if (worker)
		return worker;

ERROR:
	if (worker)
		fireperf_worker_free(worker);

	return NULL;
}

struct fireperf_stats fireperf_ctx_get_stats(struct fireperf_ctx* ctx) {
	const struct fireperf_stats* s = NULL;
	int r;

	struct fireperf_stats stats = {};

	// Fetch the time
	r = clock_gettime(CLOCK_REALTIME, &stats.t);
	if (r) {
		ERROR(ctx, "Could not fetch the time: %m\n");
		return stats;
	}

	// Add up everything from all workers
	for (unsigned int i = 0; i < ctx->num_workers; i++) {
		if (!ctx->workers[i])
			continue;

		// Fetch stats
		s = fireperf_worker_get_stats(ctx->workers[i]);
		if (!s)
			continue;

		// Sum up all open connections
		stats.open_connections += s->open_connections;

		// Sum up the transferred data
		stats.bytes_received       += s->bytes_received;
		stats.total_bytes_received += s->total_bytes_received;
		stats.bytes_sent           += s->bytes_sent;
		stats.total_bytes_sent     += s->total_bytes_sent;
	}

	return stats;
}
