/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef FIREPERF_CTX_H
#define FIREPERF_CTX_H

#include <netinet/in.h>
#include <stdarg.h>

#include "constants.h"
#include "stats.h"
#include "tui.h"

// Forward declarations
struct fireperf_worker;

typedef void (*fireperf_log_callback)(void* data, int priority, const char* file,
	int line, const char* fn, const char* format, va_list args);

struct fireperf_ctx {
	// Logging
	struct {
		fireperf_log_callback callback;
		void* data;
	} log;

	// TUI
	struct fireperf_tui* tui;

	int terminated;
	int loglevel;
	enum {
		FIREPERF_MODE_NONE = 0,
		FIREPERF_MODE_CLIENT,
		FIREPERF_MODE_SERVER,
	} mode;
	struct in6_addr address;
	int duplex;
	int keepalive_only;
	int keepalive_count;
	int keepalive_interval;
	int port;
	unsigned int listening_sockets;
	unsigned long parallel;
	unsigned int timeout;
	int close;
	int zero;

	// Random Pool
	char pool[DEFAULT_RANDOM_POOL_SIZE];

	// Workers
	struct fireperf_worker* workers[MAX_WORKERS];
	unsigned int num_workers;
	unsigned int max_workers;

	// Stats
	struct fireperf_stats stats;
};

#include "main.h"
#include "worker.h"

int fireperf_ctx_create(struct fireperf_ctx** ctx, int argc, char* argv[]);
void fireperf_ctx_free(struct fireperf_ctx* ctx);

struct fireperf_worker* fireperf_ctx_fetch_worker(struct fireperf_ctx* ctx);
struct fireperf_stats fireperf_ctx_get_stats(struct fireperf_ctx* ctx);

#endif /* FIREPERF_CTX_H */
