/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdarg.h>
#include <stdio.h>
#include <syslog.h>

#include "ctx.h"
#include "logging.h"

int fireperf_get_log_level(struct fireperf_ctx* ctx) {
	return ctx->loglevel;
}

static void fireperf_log_console(void* data, int priority,
		const char* file, int line, const char* fn, const char* format, va_list args) {
	struct fireperf_ctx* ctx = data;

	switch (priority) {
		// Print error messages to stderr
		case LOG_ERR:
			vfprintf(stderr, format, args);
			break;

		// Otherwise print to stdout
		default:
			vprintf(format, args);
			break;
	}
}

void fireperf_set_log_callback(struct fireperf_ctx* ctx,
		fireperf_log_callback callback, void* data) {
	if (!callback) {
		callback = fireperf_log_console;
		data     = ctx;
	}

	ctx->log.callback = callback;
	ctx->log.data     = data;
}

void fireperf_log(struct fireperf_ctx* ctx, int priority,
		const char* file, int line, const char* fn, const char* format, ...) {
	va_list args;

	// Return immediately if we don't have a callback registered
	if (!ctx->log.callback)
		return;

	va_start(args, format);
	ctx->log.callback(ctx->log.data, priority, file, line, fn, format, args);
	va_end(args);
}
