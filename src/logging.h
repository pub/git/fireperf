/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef FIREPERF_LOGGING_H
#define FIREPERF_LOGGING_H

#include <syslog.h>

#include "ctx.h"

static inline void __attribute__((always_inline, format(printf, 2, 3)))
	fireperf_log_null(struct fireperf_ctx* ctx, const char* format, ...) {}

#define fireperf_log_cond(ctx, prio, arg...) \
	do { \
		if (fireperf_get_log_level(ctx) >= prio) \
			fireperf_log(ctx, prio, __FILE__, __LINE__, __FUNCTION__, ## arg); \
	} while (0)


#ifdef ENABLE_DEBUG
#  define DEBUG(ctx, arg...) fireperf_log_cond(ctx, LOG_DEBUG, ## arg)
#else
#  define DEBUG(ctx, arg...) fireperf_log_null(ctx, ## arg)
#endif

#define INFO(ctx, arg...) fireperf_log_cond(ctx, LOG_INFO, ## arg)
#define ERROR(ctx, arg...) fireperf_log_cond(ctx, LOG_ERR, ## arg)

int fireperf_get_log_level(struct fireperf_ctx* ctx);

void fireperf_set_log_callback(struct fireperf_ctx* ctx,
	fireperf_log_callback callback, void* data);

void fireperf_log(struct fireperf_ctx* ctx,
	int priority, const char* file, int line, const char* fn,
	const char* format, ...) __attribute__((format(printf, 6, 7)));

#endif /* FIREPERF_LOGGING_H */
