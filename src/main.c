/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/timerfd.h>
#include <unistd.h>

#include <ncurses.h>

#include "client.h"
#include "main.h"
#include "logging.h"
#include "server.h"
#include "stats.h"

static int setup_timer(struct fireperf_ctx* ctx, int epollfd) {
	int timerfd = -1;
	int r;

	// Create timerfd()
	timerfd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK|TFD_CLOEXEC);
	if (timerfd < 0)
		return -errno;

	struct epoll_event ev = {
		.events  = EPOLLIN,
		.data.fd = timerfd,
	};

	// Register the timer with the event loop
	r = epoll_ctl(epollfd, EPOLL_CTL_ADD, timerfd, &ev);
	if (r)
		return -errno;

	// Let the timer ping us once a second
	struct itimerspec timer = {
		.it_interval.tv_sec = 1,
		.it_value.tv_sec    = 1,
	};

	// Arm the timer
	r = timerfd_settime(timerfd, 0, &timer, NULL);
	if (r)
		return -errno;

	return timerfd;
}

static int set_limits(struct fireperf_ctx* ctx) {
	struct rlimit limit;

	// Fetch current limit
	if (getrlimit(RLIMIT_NOFILE, &limit) < 0) {
		ERROR(ctx, "Could not read RLIMIT_NOFILE: %m\n");
		return 1;
	}

	// Set current value to maximum
	limit.rlim_cur = limit.rlim_max;

	// Apply the new limit
	int r = setrlimit(RLIMIT_NOFILE, &limit);
	if (r) {
		ERROR(ctx, "Could not set open file limit to %lu: %m\n",
			(unsigned long)limit.rlim_cur);
		return 1;
	}

	DEBUG(ctx, "RLIMIT_NOFILE set to %lu\n", (unsigned long)limit.rlim_cur);

	return 0;
}

int main(int argc, char* argv[]) {
	struct fireperf_ctx* ctx = NULL;
	uint64_t expirations = 0;
	void* data = NULL;
	int epollfd = -1;
	int timerfd = -1;
	int stdinfd = -1;
	int ready;
	int r;

	// Create a new context
	r = fireperf_ctx_create(&ctx, argc, argv);
	if (r)
		return r;

	switch (ctx->mode) {
		case FIREPERF_MODE_CLIENT:
		case FIREPERF_MODE_SERVER:
			break;

		case FIREPERF_MODE_NONE:
			fprintf(stderr, "No mode selected\n");
			r = 2;
			goto ERROR;
	}

	// Setup the TUI
	r = fireperf_tui_init(&ctx->tui, ctx);
	if (r)
		goto ERROR;

	// Set limits
	r = set_limits(ctx);
	if (r)
		return r;

	// Initialize epoll()
	epollfd = epoll_create1(0);
	if (epollfd < 0) {
		ERROR(ctx, "Could not initialize epoll(): %s\n", strerror(errno));
		r = 1;
		goto ERROR;
	}

	// Register the TUI with the event loop
	stdinfd = fireperf_tui_register(ctx->tui, epollfd);
	if (stdinfd < 0) {
		ERROR(ctx, "Could not register the TUI: %s\n", strerror(-stdinfd));
		goto ERROR;
	}

	// Create a timer that fires once a second
	timerfd = setup_timer(ctx, epollfd);
	if (timerfd < 0) {
		ERROR(ctx, "Could not setup timer: %s\n", strerror(-r));
		goto ERROR;
	}

	// Initialize the client/server
	switch (ctx->mode) {
		case FIREPERF_MODE_CLIENT:
			r = fireperf_client_init(ctx, &data, epollfd);
			if (r)
				goto ERROR;
			break;

		case FIREPERF_MODE_SERVER:
			r = fireperf_server_init(ctx, &data, epollfd);
			if (r)
				goto ERROR;
			break;

		default:
			abort();
	}

	struct epoll_event events[EPOLL_MAX_EVENTS];

	INFO(ctx, "Ready...\n");

	// Main loop
	for (;;) {
		ready = epoll_wait(epollfd, events, EPOLL_MAX_EVENTS, -1);
		if (ready < 1) {
			// We terminate gracefully when we receive a signal
			if (errno == EINTR)
				break;

			ERROR(ctx, "epoll_wait() failed: %m\n");
			goto ERROR;
		}

		// Iterate over all file descriptors that have activity
		for (int i = 0; i < ready; i++) {
			int fd = events[i].data.fd;

			// Handle timer events
			if (fd == timerfd) {
				// Read from the timer to disarm it
				ssize_t bytes_read = read(timerfd, &expirations, sizeof(expirations));
				if (bytes_read <= 0) {
					ERROR(ctx, "Could not read from timerfd: %s\n", strerror(errno));
					goto ERROR;
				}

				// Fetch the stats
				struct fireperf_stats stats = fireperf_ctx_get_stats(ctx);

				// Update the TUI
				r = fireperf_tui_update(ctx->tui, stats);
				if (r)
					goto ERROR;

			// Handle user input
			} else if (fd == stdinfd) {
				r = fireperf_tui_action(ctx->tui);
				if (r)
					goto ERROR;

			// Handle everything else
			} else {
				switch (ctx->mode) {
					case FIREPERF_MODE_CLIENT:
						r = fireperf_client_handle(ctx, data, fd);
						if (r)
							goto ERROR;
						break;

					case FIREPERF_MODE_SERVER:
						r = fireperf_server_handle(ctx, data, fd);
						if (r)
							goto ERROR;
						break;

					default:
						abort();
				}
			}
		}
	}

ERROR:
	// Terminate the TUI
	fireperf_tui_finish(ctx->tui);

	switch (ctx->mode) {
		case FIREPERF_MODE_CLIENT:
			if (data)
				fireperf_client_free(ctx, data);
			break;

		case FIREPERF_MODE_SERVER:
			if (data)
				fireperf_server_free(ctx, data);
			break;

		default:
			abort();
	}

	if (epollfd > 0)
		close(epollfd);
	if (timerfd > 0)
		close(timerfd);
	if (ctx)
		fireperf_ctx_free(ctx);

	return r;
}

int set_socket_buffer_sizes(struct fireperf_ctx* ctx, int fd) {
	int r;

	// Set socket buffer sizes
	int flags = SOCKET_SEND_BUFFER_SIZE;
	r = setsockopt(fd, SOL_SOCKET, SO_SNDBUF, (void*)&flags, sizeof(flags));
	if (r) {
		ERROR(ctx, "Could not set send buffer size on socket %d: %s\n",
			fd, strerror(errno));
		return 1;
	}

	// Set receive buffer size
	flags = SOCKET_RECV_BUFFER_SIZE;
	r = setsockopt(fd, SOL_SOCKET, SO_RCVBUF, (void*)&flags, sizeof(flags));
	if (r) {
		ERROR(ctx, "Could not set receive buffer size on socket %d: %s\n",
			fd, strerror(errno));
		return 1;
	}

	return 0;
}
