/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <stdlib.h>
#include <sys/random.h>

#include "ctx.h"
#include "logging.h"
#include "random.h"

int fireperf_random_pool_init(struct fireperf_ctx* ctx) {
	size_t offset = 0;

	while (offset < sizeof(ctx->pool)) {
		offset += getrandom(ctx->pool + offset, sizeof(ctx->pool) - offset, 0);
	}

	DEBUG(ctx, "Allocated random pool of %zu bytes(s)\n", sizeof(ctx->pool));

	return 0;
}

const char* fireperf_random_pool_get_slice(struct fireperf_ctx* ctx, size_t size) {
	if (size > sizeof(ctx->pool))
		return NULL;

	// Find a random value between the start and end of
	// the data region that is at least size bytes long.
	off_t offset = random() % (sizeof(ctx->pool) - size);

	return ctx->pool + offset;
}
