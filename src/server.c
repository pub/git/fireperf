/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/epoll.h>
#include <unistd.h>

#include "constants.h"
#include "ctx.h"
#include "logging.h"
#include "main.h"
#include "server.h"
#include "worker.h"

static int enable_keepalive(struct fireperf_ctx* ctx, int fd) {
	// Enable keepalive
	int flags = 1;
	int r = setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (void*)&flags, sizeof(flags));
	if (r) {
		ERROR(ctx, "Could not set SO_KEEPALIVE on socket %d: %s\n",
			fd, strerror(errno));
		return 1;
	}

	// Set keepalive interval
	if (ctx->keepalive_interval) {
		DEBUG(ctx, "Setting keepalive interval to %d\n", ctx->keepalive_interval);

		r = setsockopt(fd, SOL_TCP, TCP_KEEPINTVL,
			(void*)&ctx->keepalive_interval, sizeof(ctx->keepalive_interval));
		if (r) {
			ERROR(ctx, "Could not set TCP_KEEPINTVL on socket %d: %s\n",
				fd, strerror(errno));
			return 1;
		}

		DEBUG(ctx, "Setting keepalive idle interval to %d\n", ctx->keepalive_interval);

		flags = 1;
		r = setsockopt(fd, SOL_TCP, TCP_KEEPIDLE,
			(void*)&flags, sizeof(flags));
		if (r) {
			ERROR(ctx, "Could not set TCP_KEEPIDLE on socket %d: %s\n",
				fd, strerror(errno));
			return 1;
		}
	}

	// Set keepalive count
	if (ctx->keepalive_count) {
		DEBUG(ctx, "Setting keepalive count to %d\n", ctx->keepalive_count);

		r = setsockopt(fd, SOL_TCP, TCP_KEEPCNT,
			(void*)&ctx->keepalive_count, sizeof(ctx->keepalive_count));
		if (r) {
			ERROR(ctx, "Could not set TCP_KEEPCNT on socket %d: %s\n",
				fd, strerror(errno));
			return 1;
		}
	}

	return 0;
}

int fireperf_server_init(struct fireperf_ctx* ctx, void** data, int epollfd) {
	int flags = 1;
	int fd;
	int r;

	// Open a new socket
	fd = socket(AF_INET6, SOCK_STREAM|SOCK_NONBLOCK|SOCK_CLOEXEC, 0);
	if (fd < 0) {
		ERROR(ctx, "Could not open socket: %s\n", strerror(errno));
		goto ERROR;
	}

#if 1
	// Enable to re-use the address
	r = setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &flags, sizeof(flags));
	if (r) {
		ERROR(ctx, "Could not set SO_REUSEADDR on socket %d: %s\n",
			fd, strerror(errno));
		goto ERROR;
	}

	// Enable to re-use the port
	r = setsockopt(fd, SOL_SOCKET, SO_REUSEPORT, &flags, sizeof(flags));
	if (r) {
		ERROR(ctx, "Could not set SO_REUSEPORT on socket %d: %s\n",
			fd, strerror(errno));
		goto ERROR;
	}
#endif

	// Enable zero-copy
	r = setsockopt(fd, SOL_SOCKET, SO_ZEROCOPY, &flags, sizeof(flags));
	if (r) {
		ERROR(ctx, "Could not set SO_ZEROCOPY on socket %d: %s\n",
			fd, strerror(errno));
		goto ERROR;
	}

	// Set socket buffer sizes
	r = set_socket_buffer_sizes(ctx, fd);
	if (r)
		goto ERROR;

	// Enable keepalive
	if (ctx->keepalive_only) {
		r = enable_keepalive(ctx, fd);
		if (r)
			goto ERROR;
	}

	struct sockaddr_in6 addr = {
		.sin6_family = AF_INET6,
		.sin6_port   = htons(ctx->port),
	};

	// Bind it to the selected port
	r = bind(fd, &addr, sizeof(addr));
	if (r) {
		ERROR(ctx, "Could not bind socket: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Listen
	r = listen(fd, SOCKET_BACKLOG);
	if (r) {
		ERROR(ctx, "Could not listen on socket: %m\n");
		r = -errno;
		goto ERROR;
	}

	// Add listening socket to epoll
	struct epoll_event ev = {
		.events  = EPOLLIN,
		.data.fd = fd,
	};

	// Add the socket to the event loop
	r = epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev);
	if (r) {
		ERROR(ctx, "Could not add socket file descriptor to epoll(): %m\n");
		r = -errno;
		goto ERROR;
	}

	DEBUG(ctx, "Created listening socket %d\n", fd);

	return 0;

ERROR:
	close(fd);

	return r;
}

int fireperf_server_handle(struct fireperf_ctx* ctx, void* data, int sockfd) {
	struct fireperf_worker* worker = NULL;
	struct sockaddr_in6 addr = {};
	int fd = -1;
	int r = 0;

	socklen_t l = sizeof(addr);

	// The listening socket is ready, there is a new connection waiting to be accepted
	do {
		fd = accept(sockfd, &addr, &l);
	} while (fd < 0 && (errno == EAGAIN || errno == EWOULDBLOCK));

	// Fail if we could not accept the connection
	if (fd < 0) {
		ERROR(ctx, "Could not accept a new connection: %s\n", strerror(errno));
		return -errno;
	}

	DEBUG(ctx, "New connection accepted on socket %d\n", fd);

	// Close connections immediately when -x is set
	if (ctx->close)
		goto ERROR;

	// Find a worker to delegate this connection to
	worker = fireperf_ctx_fetch_worker(ctx);

	// Close the connection if we could not find a worker
	if (!worker) {
		ERROR(ctx, "Could not find a worker that could handle a new connection\n");
		r = -EBUSY;
		goto ERROR;
	}

	// Read any data
	int events = FIREPERF_WORKER_RECV;

	// Send data unless we are in keepalive-only mode
	if (!ctx->keepalive_only)
		events |= FIREPERF_WORKER_SEND;

	// Delegate the connection
	r = fireperf_worker_delegate(worker, events, fd);
	if (r < 0) {
		ERROR(ctx, "Could not delegate a new connection to a worker: %s\n", strerror(-r));
		goto ERROR;
	}

	return 0;

ERROR:
	DEBUG(ctx, "Closing connection %d\n", fd);
	close(fd);

	return r;
}

void fireperf_server_free(struct fireperf_ctx*, void* data) {
	return;
}
