/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "ctx.h"
#include "logging.h"
#include "stats.h"
#include "util.h"

struct fireperf_stats fireperf_stats_step(
		const struct fireperf_stats* old, const struct fireperf_stats* new) {
	struct fireperf_stats stats = {
		.t                    = new->t,
		.open_connections     = new->open_connections,
		.connections          = new->connections - ((old) ? old->connections : 0),
		.total_bytes_received = new->total_bytes_received,
		.bytes_received       = new->total_bytes_received - ((old) ? old->total_bytes_received : 0),
		.total_bytes_sent     = new->total_bytes_sent,
		.bytes_sent           = new->total_bytes_sent - ((old) ? old->total_bytes_sent : 0),
	};

	return stats;
}
