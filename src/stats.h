/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef FIREPERF_STATS_H
#define FIREPERF_STATS_H

#include <time.h>

// Struct to collect statistics
struct fireperf_stats {
	struct timespec t;

	// Total number of open connections
	unsigned int open_connections;

	// New connections
	unsigned int connections;

	// Total transferred data
	size_t bytes_received;
	size_t total_bytes_received;
	size_t bytes_sent;
	size_t total_bytes_sent;
};

struct fireperf_stats fireperf_stats_step(
	const struct fireperf_stats* old, const struct fireperf_stats* new);

#endif /* FIREPERF_STATS_H */
