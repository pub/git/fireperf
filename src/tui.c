/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <unistd.h>

#include <ncurses.h>

#include "ctx.h"
#include "logging.h"
#include "stats.h"
#include "tui.h"
#include "util.h"

#define MAX_STATS 1024

enum {
	FIREPERF_RECV,
	FIREPERF_SENT,
};

struct fireperf_tui {
	struct fireperf_ctx* ctx;

	// Stats
	struct fireperf_stats stats[MAX_STATS];
	unsigned int s;

	// The main screen
	WINDOW* screen;

	// The frame around the screen
	WINDOW* frame;

	// The graph inside the frame
	WINDOW* graph;

	// The log window
	WINDOW* log;

	// The status bar
	WINDOW* status;
};

void fireperf_tui_log(void* data, int priority, const char* file,
		int line, const char* fn, const char* format, va_list args) {
	struct fireperf_tui* tui = data;
	char buffer[1024];
	ssize_t length;
	int rows;
	int cols;

	// Fetch the dimensions of the log window
	getmaxyx(tui->log, rows, cols);

	// We leave one character free on the left and the right
	cols -= 2;

	// Format the message
	length = vsnprintf(buffer, sizeof(buffer), format, args);
	if (length < 0)
		return;

	// Scroll down the previous content to add an extra line
	scroll(tui->log);

	// Remove the trailing newline
	length--;

	// If the message is long
	if (length > cols)
		length = cols;

	// Write the message
	mvwaddnstr(tui->log, rows - 1, 1, buffer, length);

	wrefresh(tui->log);

	return;
}

static const struct fireperf_stats* fireperf_tui_get_stats(struct fireperf_tui* tui, int i) {
	// Select the i'th most recent index
	i = (tui->s - i) % MAX_STATS;

	const struct fireperf_stats* stats = &tui->stats[i];

	// Return NULL if we have no meaningful data
	if (!stats->total_bytes_received && !stats->total_bytes_sent)
		return NULL;

	return stats;
}

static size_t fireperf_tui_get_peak_bps(struct fireperf_tui* tui, int max) {
	const struct fireperf_stats* stats = NULL;
	size_t peak = 0;

	for (unsigned int i = 0; i < max; i++) {
		stats = fireperf_tui_get_stats(tui, i);
		if (!stats)
			continue;

		if (stats->bytes_sent > peak)
			peak = stats->bytes_sent;

		if (stats->bytes_received > peak)
			peak = stats->bytes_received;
	}

	return peak;
}

static size_t fireperf_tui_get_avg_bytes(
		struct fireperf_tui* tui, const int direction, int max) {
	const struct fireperf_stats* stats = NULL;

	size_t counter = 0;
	size_t total_bytes = 0;

	for (unsigned int i = 0; i < max; i++) {
		stats = fireperf_tui_get_stats(tui, i);
		if (!stats)
			continue;

		counter++;

		switch (direction) {
			case FIREPERF_RECV:
				total_bytes += stats->bytes_received;
				break;

			case FIREPERF_SENT:
				total_bytes += stats->bytes_sent;
				break;
		}
	}

	if (!counter)
		return 0;

	return total_bytes / counter;
}

static int fireperf_tui_setup_graph(struct fireperf_tui* tui) {
	int r;

	// Create the graph
	tui->graph = newwin(LINES - 13, COLS - 3, 1, 1);
	if (!tui->graph)
		return 1;

	// Refresh
	wrefresh(tui->graph);

	return 0;
}

static int fireperf_tui_setup_frame(struct fireperf_tui* tui) {
	int r;

	// Create the window
	tui->frame = newwin(LINES - 11, COLS, 0, 0);
	if (!tui->frame)
		return 1;

	// Make my brush blue
	wattron(tui->frame, COLOR_PAIR(3));

	// Draw a box around the frame
	box(tui->frame, 0, 0);

	// Reset my brush
	wattroff(tui->frame, COLOR_PAIR(3));

	// Refresh
	wrefresh(tui->frame);

	return 0;
}

static int fireperf_tui_update_status(struct fireperf_tui* tui) {
	const struct fireperf_stats* stats = NULL;
	char buffer[32];
	int r;

	int max_x = 0;
	int max_y = 0;

	// Fetch the dimensions of the status bar
	getmaxyx(tui->status, max_y, max_x);

	// Status on the left and right
	char* status_l = NULL;
	char* status_r = NULL;

	// Fetch the latest stats
	stats = fireperf_tui_get_stats(tui, 0);
	if (!stats)
		goto ERROR;

	// Print some help text on the right
	r = asprintf(&status_r, "Press 'q' to quit");
	if (r < 0)
		goto ERROR;

	// Print some connection information on the left
	r = asprintf(&status_l, "%zu Open Connection(s), %zu Total Connection(s)",
			stats->open_connections, stats->connections);
	if (r < 0)
		goto ERROR;

	// Fetch peak bandwidth
	size_t peak_bps = fireperf_tui_get_peak_bps(tui, max_x);

	// Format peak bandwidth
	r = format_size(buffer, peak_bps * 8, FIREPERF_FORMAT_BITS_PER_SECOND);
	if (r < 0)
		goto ERROR;

	// Print Peak Bandwidth
	r = asprintf(&status_l, "%s, Peak Bandwidth: %s", status_l, buffer);
	if (r < 0)
		goto ERROR;

	// Erase the previous content
	werase(tui->status);

	// Write in black & white
	wattron(tui->status, COLOR_PAIR(4));

	// Fill everything with the background color
	for (unsigned int i = 0; i < COLS; i++)
		waddch(tui->status, ' ');

	// Write the text on the left
	if (status_l)
		mvwaddstr(tui->status, 0, 1, status_l);

	// Write the text on the right
	if (status_r)
		mvwaddstr(tui->status, 0, COLS - strlen(status_r) - 2, status_r);

	waddch(tui->status, ' ');

	// Reset the colour
	wattroff(tui->status, COLOR_PAIR(4));

	// Refresh the view
	wrefresh(tui->status);

ERROR:
	if (status_l)
		free(status_l);
	if (status_r)
		free(status_r);

	return 0;
}

static int fireperf_tui_setup_log(struct fireperf_tui* tui) {
	int r;

	// Create the window
	tui->log = newwin(10, COLS, LINES - 11, 0);
	if (!tui->log)
		return 1;

	// Enable scrolling in the window
	scrollok(tui->log, 1);

	// Refresh
	wrefresh(tui->log);

	return 0;
}

static int fireperf_tui_setup_status(struct fireperf_tui* tui) {
	int r;

	// Create the window
	tui->status = newwin(1, COLS, LINES - 1, 0);
	if (!tui->status)
		return 1;

	// Perform an initial drawing
	r = fireperf_tui_update_status(tui);
	if (r)
		return r;

	// Refresh
	wrefresh(tui->status);

	return 0;
}

static int fireperf_tui_setup(struct fireperf_tui* tui) {
	int r;

	// Start ncurses
	tui->screen = initscr();
	if (!tui->screen)
		return -errno;

	// We would like to see colours
	start_color();

	// Configure a few colours
	init_pair(1, COLOR_RED,    COLOR_BLACK);
	init_pair(2, COLOR_GREEN,  COLOR_BLACK);
	init_pair(3, COLOR_BLUE,   COLOR_BLACK);
	init_pair(4, COLOR_BLACK,  COLOR_WHITE);
	init_pair(5, COLOR_YELLOW, COLOR_BLACK);

	// Enable raw mode
	raw();

	// Disable character echoing
	noecho();

	// Set the cursor to the top
	curs_set(0);

	// Configure the keyboard
	keypad(stdscr, 1);

	// ???
	halfdelay(1);

	// Never block
	timeout(0);

	// Refresh!
	refresh();

	// Setup the frame
	r = fireperf_tui_setup_frame(tui);
	if (r)
		return r;

	// Setup the graph
	r = fireperf_tui_setup_graph(tui);
	if (r)
		return r;

	// Setup the log window
	r = fireperf_tui_setup_log(tui);
	if (r)
		return r;

	// Setup the status bar
	r = fireperf_tui_setup_status(tui);
	if (r)
		return r;

	// Refresh!
	refresh();

	return 0;
}

int fireperf_tui_init(struct fireperf_tui** tui, struct fireperf_ctx* ctx) {
	struct fireperf_tui* t = NULL;
	int r;

	// Allocate the TUI
	t = calloc(1, sizeof(*t));
	if (!t)
		return -errno;

	// Store a reference to the context
	t->ctx = ctx;

	// Perform the basic setup
	r = fireperf_tui_setup(t);
	if (r)
		return r;

	// Hijack log messages
	fireperf_set_log_callback(ctx, fireperf_tui_log, t);

	// Return the TUI
	*tui = t;

	return 0;
}

void fireperf_tui_finish(struct fireperf_tui* tui) {
	endwin();

	free(tui);
}

int fireperf_tui_register(struct fireperf_tui* tui, int epollfd) {
	const int fd = STDIN_FILENO;
	int r;

	struct epoll_event ev = {
		.events  = EPOLLIN|EPOLLET,
		.data.fd = fd,
	};

	// Register the timer with the event loop
	r = epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev);
	if (r)
		return -errno;

	return fd;
}

/*
	Called when the user presses a button
*/
int fireperf_tui_action(struct fireperf_tui* tui) {
	int c;

	for (;;) {
		// Fetch the next character
		c = getch();
		if (c < 0)
			break;

		switch (c) {
			// Quit
			case 'q':
				return -ESHUTDOWN;

			default:
				break;
		}
	}

	return 0;
}

static int fireperf_tui_draw_line(struct fireperf_tui* tui, double value, const char* label) {
	size_t l = 0;

	// How long is the label?
	if (label)
		l = strlen(label);

	int max_x = 0;
	int max_y = 0;

	// Fetch the dimensions of the frame
	getmaxyx(tui->graph, max_y, max_x);

	// Draw a horizontal line
	for (unsigned int x = 0; x < max_x - l - 1; x++) {
		mvwaddch(tui->graph, max_y - value, x, ACS_HLINE);
	}

	// Write the label
	if (label)
		mvwaddnstr(tui->graph, max_y - value, max_x - l, label, l);

	return 0;
}

/*
	Draws the big graph
*/
static int fireperf_tui_draw_graph(struct fireperf_tui* tui) {
	const struct fireperf_stats* stats = NULL;
	char label[32];
	int r;

	int max_x = 0;
	int max_y = 0;

	// Erase any previous content
	werase(tui->graph);

	// Fetch the dimensions of the frame
	getmaxyx(tui->graph, max_y, max_x);

	// Fetch total transfer
	size_t avg_rcvd_bytes = fireperf_tui_get_avg_bytes(tui, FIREPERF_RECV, max_x);
	size_t avg_sent_bytes = fireperf_tui_get_avg_bytes(tui, FIREPERF_SENT, max_x);

	// Fetch peak bandwidth
	size_t peak_bps = fireperf_tui_get_peak_bps(tui, max_x);

	// Figure out how many bps a step represents
	double step = peak_bps / (max_y - 4);

	// Make the brush yellow
	wattron(tui->graph, COLOR_PAIR(5));

	// Draw the grid
	for (double line = 0; line <= 1; line += 0.25) {
		r = format_size(label, line * peak_bps * 8, FIREPERF_FORMAT_BITS_PER_SECOND);
		if (r < 0)
			continue;

		// Write the line
		r = fireperf_tui_draw_line(tui, max_y * line, label);
		if (r)
			return r;
	}

	wattroff(tui->graph, COLOR_PAIR(5));

	// Make the brush red
	wattron(tui->graph, COLOR_PAIR(1));

	// Show the average
	if (avg_sent_bytes) {
		r = format_size(label, avg_sent_bytes * 8, FIREPERF_FORMAT_BITS_PER_SECOND);
		if (r < 0)
			return r;

		// Write the line
		r = fireperf_tui_draw_line(tui, avg_sent_bytes / step, label);
		if (r)
			return r;
	}

	// Draw any sent traffic
	for (unsigned int x = 0; x < max_x; x++) {
		stats = fireperf_tui_get_stats(tui, x);
		if (!stats)
			continue;

		mvwaddch(tui->graph, max_y - (stats->bytes_sent / step), x, '#');
	}

	wattroff(tui->graph, COLOR_PAIR(1));

	// Make the brush green
	wattron(tui->graph, COLOR_PAIR(2));

	// Show the average
	if (avg_rcvd_bytes) {
		r = format_size(label, avg_rcvd_bytes * 8, FIREPERF_FORMAT_BITS_PER_SECOND);
		if (r < 0)
			return r;

		// Write the line
		r = fireperf_tui_draw_line(tui, avg_rcvd_bytes / step, label);
		if (r)
			return r;
	}

	// Draw any received traffic
	for (unsigned int x = 0; x < max_x; x++) {
		stats = fireperf_tui_get_stats(tui, x);
		if (!stats)
			continue;

		mvwaddch(tui->graph, max_y - (stats->bytes_received / step), x, '#');
	}

	wattroff(tui->graph, COLOR_PAIR(2));

	wrefresh(tui->graph);

	return 0;
}

/*
	Called when there is new data to update the UI
*/
int fireperf_tui_update(struct fireperf_tui* tui, struct fireperf_stats stats) {
	int r;

	// Fetch the previous stats
	const struct fireperf_stats* prev_stats = fireperf_tui_get_stats(tui, 1);

	// Store stats
	tui->stats[tui->s++] = fireperf_stats_step(prev_stats, &stats);

	// Wrap the pointer around
	tui->s %= MAX_STATS;

	// Draw the graph
	r = fireperf_tui_draw_graph(tui);
	if (r)
		return r;

	// Update the status
	return fireperf_tui_update_status(tui);
}
