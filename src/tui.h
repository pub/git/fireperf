/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef FIREPERF_TUI_H
#define FIREPERF_TUI_H

struct fireperf_tui;

#include "ctx.h"
#include "stats.h"

// Forward declaration
struct fireperf_ctx;

int fireperf_tui_init(struct fireperf_tui** tui, struct fireperf_ctx* ctx);
void fireperf_tui_finish(struct fireperf_tui* tui);

int fireperf_tui_register(struct fireperf_tui* tui, int epollfd);
int fireperf_tui_action(struct fireperf_tui* tui);

int fireperf_tui_update(struct fireperf_tui* tui, struct fireperf_stats stats);

#endif /* FIREPERF_TUI_H */
