/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "util.h"

static const char* units_bytes[] = { "B", "KiB", "MiB", "GiB", "TiB", NULL };
static const char* units_bits[]  = { "b", "Kb", "Mb", "Gb", "Tb", NULL };

int __format_size(char* buffer, const size_t length, ssize_t size, int format) {
	const char** units = NULL;
	unsigned int divisor;
	int r;

	const char* suffix = "";

	switch (format) {
		case FIREPERF_FORMAT_BYTES_PER_SECOND:
			format = FIREPERF_FORMAT_BYTES;
			suffix = "/s";
			break;

		case FIREPERF_FORMAT_BITS_PER_SECOND:
			format = FIREPERF_FORMAT_BITS;
			suffix = "/s";
			break;
	}

	switch (format) {
		case FIREPERF_FORMAT_BYTES:
			units = units_bytes;
			divisor = 1024;
			break;

		case FIREPERF_FORMAT_BITS:
			units = units_bits;
			divisor = 1000;
			break;

		// Invalid input
		default:
			return -EINVAL;
	}

	const char** unit = NULL;

	// Convert into double
	double s = size;

	for (unit = units; *unit; unit++) {
		if (abs(s) < divisor)
			break;

		s /= divisor;
	}

	if (!*unit)
		return -EINVAL;

	// Format the output string
	r = snprintf(buffer, length, "%.02f %s%s", s, *unit, suffix);
	if (r < 0)
		return r;

	return 0;
}

const char* format_timespec(const struct timespec* t) {
	static char __thread __timespec[20];

	// Convert to local time
	struct tm* tm = localtime(&t->tv_sec);

	size_t s = strftime(__timespec, sizeof(__timespec), "%F %T", tm);

	if (s)
		return __timespec;

	return NULL;
}

double timespec_delta(struct timespec* t1, struct timespec* t2) {
	// Compute delta in seconds
	return (
		((t1->tv_sec * 1000) + (t1->tv_nsec / 1000000))
		-
		((t2->tv_sec * 1000) + (t2->tv_nsec / 1000000))
	) / 1000.0;
}
