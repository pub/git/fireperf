/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2021 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <time.h>
#include <unistd.h>

enum {
	FIREPERF_FORMAT_BYTES,
	FIREPERF_FORMAT_BYTES_PER_SECOND,
	FIREPERF_FORMAT_BITS,
	FIREPERF_FORMAT_BITS_PER_SECOND,
};

#define format_size(buffer, size, unit) \
	__format_size(buffer, sizeof(buffer), size, unit)

int __format_size(char* buffer, const size_t length, ssize_t size, int format);

const char* format_timespec(const struct timespec* t);
double timespec_delta(struct timespec* t1, struct timespec* t2);
