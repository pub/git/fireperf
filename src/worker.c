/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#include <errno.h>
#include <poll.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <liburing.h>

#include "constants.h"
#include "ctx.h"
#include "main.h"
#include "logging.h"
#include "random.h"
#include "stats.h"
#include "worker.h"

#define MAX_CONNECTIONS 128

static const char ZERO[SOCKET_SEND_BUFFER_SIZE] = { 0 };

enum {
	FIREPERF_EVENT_SEND = 1,
	FIREPERF_EVENT_SENT = 2,
	FIREPERF_EVENT_RECV = 3,
};

#define EVENT(type, fd)   (void*)(uint64_t)((fd << 8) | type)

#define EVENT_GET_TYPE(e) ((uint64_t)e & 0xff)
#define EVENT_GET_FD(e)   ((uint64_t)e >> 8)

struct fireperf_worker {
	pthread_t thread;

	// Configuration
	struct fireperf_ctx* ctx;

	// Collect stats
	struct fireperf_stats stats;

	// IO uring
	struct io_uring ring;

	// Receive Buffer (1 MiB)
	char recv_buffer[1 * 1024 * 1024];

	// Exit Code
	int r;
};

static int fireperf_worker_register_recv_buffer(struct fireperf_worker* worker) {
	const struct iovec recv_buffer = {
		.iov_base = worker->recv_buffer,
		.iov_len = sizeof(worker->recv_buffer),
	};
	int r;

	// Register for zero-copy
	r = io_uring_register_buffers(&worker->ring, &recv_buffer, 1);
	if (r < 0)
		ERROR(worker->ctx, "Could not register the receive buffer: %s\n", strerror(-r));

	return r;
}

int fireperf_worker_create(struct fireperf_worker** worker, struct fireperf_ctx* ctx) {
	struct fireperf_worker* w = NULL;
	int r;

	// Allocate a new worker
	w = calloc(1, sizeof(*w));
	if (!w)
		return -errno;

	// Reference the configuration
	w->ctx = ctx;

	// Setup ring
	r = io_uring_queue_init(512, &w->ring, 0);
	if (r) {
		ERROR(w->ctx, "Could not set up IO uring: %s\n", strerror(r));
		goto ERROR;
	}

	// Register the receive buffer
	r = fireperf_worker_register_recv_buffer(w);
	if (r)
		goto ERROR;

	DEBUG(ctx, "Created a new worker\n");

	// Return the worker
	*worker = w;

	return 0;

ERROR:
	if (w)
		fireperf_worker_free(w);

	return r;
}

void fireperf_worker_free(struct fireperf_worker* worker) {
	io_uring_queue_exit(&worker->ring);

	free(worker);
}

static int fireperf_worker_register_send(struct fireperf_worker* worker, const int sockfd) {
	int r;

	// Fetch a new submission queue entry
	struct io_uring_sqe *sqe = io_uring_get_sqe(&worker->ring);
	if (!sqe)
		return -ENOSPC;

	// Poll for when the socket is ready to write to
	io_uring_prep_poll_add(sqe, sockfd, POLLOUT);

	// Store the event
	io_uring_sqe_set_data(sqe, EVENT(FIREPERF_EVENT_SEND, sockfd));

	return 0;
}

static int fireperf_worker_send(
		struct fireperf_worker* worker, const struct io_uring_cqe* cqe, int sockfd) {
	const char* buffer = ZERO;
	ssize_t bytes_sent;
	int r;

	// Fetch a new submission queue entry
	struct io_uring_sqe *sqe = io_uring_get_sqe(&worker->ring);
	if (!sqe)
		return -ENOSPC;

	// Fetch some random data if requested
	if (!worker->ctx->zero)
		buffer = fireperf_random_pool_get_slice(worker->ctx, SOCKET_SEND_BUFFER_SIZE);

	// Send a chunk of data
	io_uring_prep_send_zc(sqe, sockfd, buffer, SOCKET_SEND_BUFFER_SIZE, 0, 0);

	// Store the event
	io_uring_sqe_set_data(sqe, EVENT(FIREPERF_EVENT_SENT, sockfd));

	return 0;
}

static int fireperf_worker_sent(
		struct fireperf_worker* worker, const struct io_uring_cqe* cqe, int sockfd) {
	// Handle errors
	if (cqe->res < 0) {
		ERROR(worker->ctx, "Could not send to %d: %s\n", sockfd, strerror(-cqe->res));
		return cqe->res;
	}

	if (cqe->res == 0)
		return 0;

	DEBUG(worker->ctx, "Sent %zu bytes to socket %d\n", cqe->res, sockfd);

	// Update statistics
	worker->stats.bytes_sent       += cqe->res;
	worker->stats.total_bytes_sent += cqe->res;

	// Ask to send more data
	return fireperf_worker_register_send(worker, sockfd);
}

static int fireperf_worker_register_recv(struct fireperf_worker* worker, const int sockfd) {
	int r;

	// Fetch a new submission queue entry
	struct io_uring_sqe *sqe = io_uring_get_sqe(&worker->ring);
	if (!sqe)
		return -ENOSPC;

	io_uring_prep_recv(sqe, sockfd, worker->recv_buffer, 0, IOSQE_BUFFER_SELECT);

	// Store the event
	io_uring_sqe_set_data(sqe, EVENT(FIREPERF_EVENT_RECV, sockfd));

	return 0;
}

static int fireperf_worker_recv(
		struct fireperf_worker* worker, const struct io_uring_cqe* cqe, int sockfd) {
	size_t bytes_read = cqe->res;

	// Did we encounter an error?
	if (cqe->res < 0) {
		ERROR(worker->ctx, "Could not read from socket %d: %s\n", sockfd, strerror(-cqe->res));
		return cqe->res;
	}

	DEBUG(worker->ctx, "Read %zu bytes from socket %d\n", bytes_read, sockfd);

	// Update statistics
	worker->stats.bytes_received       += bytes_read;
	worker->stats.total_bytes_received += bytes_read;

	// Ask to receive more data
	return fireperf_worker_register_recv(worker, sockfd);
}

/*
	The main function of the worker.
*/
static void* fireperf_worker_main(void* w) {
	struct fireperf_worker* worker = w;
	int ready = 0;
	int r;

	DEBUG(worker->ctx, "New worker launched as %lu\n", pthread_self());

	struct io_uring_cqe* cqe = NULL;

	unsigned int head = 0;

	// Enter the main loop...
	for (;;) {
		// Submit the submission queue and wait for anything to finish
		r = io_uring_submit_and_wait(&worker->ring, 1);
		if (r < 0) {
			ERROR(worker->ctx, "io_uring_submit_and_wait() failed: %m\n");
			r = -errno;
			goto ERROR;
		}

		unsigned int events = 0;

		// Process everything in the completion queue
		io_uring_for_each_cqe(&worker->ring, head, cqe) {
			// Fetch the event
			void* event = io_uring_cqe_get_data(cqe);

			// Fetch the file descriptor
			int fd = EVENT_GET_FD(event);

			//DEBUG(worker->ctx, "Handling event %d\n", EVENT_GET_TYPE(event));

			switch (EVENT_GET_TYPE(event)) {
				case FIREPERF_EVENT_SEND:
					r = fireperf_worker_send(worker, cqe, fd);
					if (r)
						goto ERROR;
					break;

				case FIREPERF_EVENT_SENT:
					r = fireperf_worker_sent(worker, cqe, fd);
					if (r)
						goto ERROR;
					break;

				case FIREPERF_EVENT_RECV:
					r = fireperf_worker_send(worker, cqe, fd);
					if (r)
						goto ERROR;
					break;

				// Log unhandled events
				default:
					DEBUG(worker->ctx, "Unhandled event %d\n", EVENT_GET_TYPE(event));
					break;
			}

			// Count all successfully handled events
			events++;
		}

		DEBUG(worker->ctx, "Handled %zu event(s)\n", events);

		// Mark them all as done
		io_uring_cq_advance(&worker->ring, events);
	}

	DEBUG(worker->ctx, "Worker has gracefully terminated\n");

ERROR:
	// Store the return code
	worker->r = r;

	return &worker->r;
}

int fireperf_worker_launch(struct fireperf_worker* worker) {
	int r;

	// Launch the worker
	r = pthread_create(&worker->thread, NULL, fireperf_worker_main, worker);
	if (r) {
		ERROR(worker->ctx, "Could not launch the worker: %m\n");
	}

	return r;
}

int fireperf_worker_terminate(struct fireperf_worker* worker) {
	// If the worker has terminated, we return its exit code
	if (!fireperf_worker_is_alive(worker))
		return worker->r;

	// XXX Otherwise we need to do something else
	return 0;
}

int fireperf_worker_is_alive(struct fireperf_worker* worker) {
	return pthread_tryjoin_np(worker->thread, NULL);
}

int fireperf_worker_delegate(struct fireperf_worker* worker, int events, int sockfd) {
	int r;

	// Some event must be requested
	if (!events)
		return -EINVAL;

	// Send data?
	if (events & FIREPERF_WORKER_SEND) {
		r = fireperf_worker_register_send(worker, sockfd);
		if (r)
			return r;
	}

	// Receive data?
	if (events & FIREPERF_WORKER_RECV) {
		r = fireperf_worker_register_recv(worker, sockfd);
		if (r)
			return r;
	}

	// Submit the request(s)
	r = io_uring_submit(&worker->ring);
	if (r < 0) {
		ERROR(worker->ctx, "Could not submit socket %d: %s\n", strerror(-r));
		return r;
	}

	// We now have one more open connections
	worker->stats.open_connections++;

	return 0;
}

const struct fireperf_stats* fireperf_worker_get_stats(struct fireperf_worker* worker) {
	return &worker->stats;
}
