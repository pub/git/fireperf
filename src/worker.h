/*#############################################################################
#                                                                             #
# fireperf - A network benchmarking tool                                      #
# Copyright (C) 2024 IPFire Development Team                                  #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU General Public License as published by        #
# the Free Software Foundation, either version 3 of the License, or           #
# (at your option) any later version.                                         #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU General Public License for more details.                                #
#                                                                             #
# You should have received a copy of the GNU General Public License           #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.       #
#                                                                             #
#############################################################################*/

#ifndef FIREPERF_WORKER_H
#define FIREPERF_WORKER_H

struct fireperf_worker;

#include "ctx.h"

enum {
	FIREPERF_WORKER_SEND = (1 << 0),
	FIREPERF_WORKER_RECV = (1 << 1),
};

int fireperf_worker_create(struct fireperf_worker** worker, struct fireperf_ctx* ctx);
void fireperf_worker_free(struct fireperf_worker* worker);

int fireperf_worker_launch(struct fireperf_worker* worker);
int fireperf_worker_terminate(struct fireperf_worker* worker);
int fireperf_worker_is_alive(struct fireperf_worker* worker);

int fireperf_worker_delegate(struct fireperf_worker* worker, int events, int socket);

const struct fireperf_stats* fireperf_worker_get_stats(struct fireperf_worker* worker);

#endif /* FIREPERF_WORKER_H */
